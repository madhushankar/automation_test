package pages;


import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;


/**
 * This abstract class that every test page will implement. Sets up
 * some covenient methods for sending text to objects on the page
 * and clicking links
 *
 * @author manikandan
 */
public class Page {

    protected static AppiumDriver driver;
    protected WebDriverWait wait;
    protected AppiumDriver appiumDriver;

    /**
     * @param driver
     */
    public Page(AppiumDriver driver) {

        this.driver = driver;
    }

    protected Page() {
    }

    /**
     * send the specific text to specific locator
     *
     * @param locator
     * @param text
     * @return
     */

    protected Page sendText(By locator, String text) {
        click(locator);
        driver.findElement(locator).clear();
        driver.findElement(locator).sendKeys(new String[]{text});
        return this;
    }

    /**
     * click specific locator
     *
     * @param locator
     * @return
     */
    protected Page click(By locator) {
        driver.findElement(locator).click();
        return this;
    }

    /**
     * Some forms do not have an explicit submit button but sending an enter key
     * to the field will submit it
     *
     * @param
     * @return
     */
    protected Page enter() {
        driver.sendKeyEvent(66);
        return this;
    }

    public Page load() throws InterruptedException {
        Thread.sleep(2000);
        return this;
    }


    /**
     * go previous page from any specific page
     *
     * @return
     */
    protected Page clickBack() {
        driver.navigate().back();
        return this;
    }

    /**
     * get text from specific locator
     *
     * @param locator
     * @return text
     */
    protected String getText(By locator) {

        return driver.findElement(locator).getText();
    }

    /**
     * wait for specific locator
     *
     * @param locator
     * @return WebElement
     */
    protected WebElement waitForElement(By locator) {
        wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    /**
     * @param name
     * @return
     */
    protected Page click(String name) {

        return click(By.name(name));
    }

    protected Page clickText(String name) {
        return click(By.name(name));
    }


    /**
     * @param locator
     * @param text
     * @param direction
     * @return
     */

    protected Page scrollTo(By locator, String text, String direction) {

        WebElement element = driver.findElement(locator);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        java.util.HashMap<String, String> scrollObject = new java.util.HashMap<String, String>();

        scrollObject.put("direction", direction);

        scrollObject.put("text", text);

        scrollObject.put("element", ((RemoteWebElement) element).getId());

        js.executeScript("mobile: scrollTo", scrollObject);


        return this;
    }

    protected Page scrollToExact(By locator) {
        WebElement element = driver.findElement(locator);
        String text = element.toString();
        driver.scrollToExact(text);
        return this;
    }

    public Page clickSpecificString(String productName, List<WebElement> categoryList) {
        for (int i = 0; i < categoryList.size(); i++) {
            if (categoryList.get(i).getText().contains(productName)) {
                categoryList.get(i).click();

            }
        }
        return this;
    }

    public Page verify(By locator, String expected) {
        String actual = driver.findElement(locator).getText();

        try {
            Assert.assertTrue(actual.contains(expected), "User name not match");
            //Assert.assertEquals(actual, expected,"equal");
            System.out.println(actual + "matches");
        } catch (Throwable t) {
            System.out.println("both are not equal");
        }

        return this;
    }

}
