package pages;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;


/**
 * Created by Brajesh on 13/02/15.
 */
public class LoginPage extends Page {
    By settings = By.id("com.flipkart.android:id/mainMenu");
    By login = By.name("Login");
    By user = By.id("com.flipkart.android:id/user_id");
    By passwrd = By.id("com.flipkart.android:id/user_password");
    By showPasswrd = By.name("Show Password");
    By loginButton = By.id("com.flipkart.android:id/btn_login");
    By forgotPasswrd = By.name("Forgot Password?");
    By signUp = By.name("Sign Up");
    By facebookLogin = By.name("Facebook");
    By googleLoging = By.name("Google");
    By eMail = By.xpath("//android.webkit.WebView[1]/android.view.View[1]/android.view.View[2]/android.widget.EditText[1]");
    By send_Mail = By.name("Send Email");
    By actualuser = By.name("madhushankar...");
    By logout = By.name("Logout");
    By err_msg = By.id("com.flipkart.android:id/text");
    By clear_user = By.xpath("//android.widget.ScrollView//android.widget.LinearLayout[contains(@content-desc,'email_textbox')]" +
            "/android.widget.ImageView");
    By clear_pwd = By.xpath("//android.widget.ScrollView//android.widget.LinearLayout[contains(@content-desc,'password_textbox')]" +
            "/android.widget.ImageView");

    public LoginPage(AppiumDriver driver) {
        super(driver);
    }

    public void setting() {
        click(settings);
    }

    public void navigateToLogin() {
        click(login);
    }

    public void login(String username, String password) throws InterruptedException {
        sendText(user, username);
        sendText(passwrd, password);
        click(loginButton);


    }

    public void forgotPassword(String email) {
        click(forgotPasswrd);
        sendText(eMail, email);
        click(send_Mail);
    }

    public void verifyuser(String user) {
       /* WebElement actual=driver.findElement(actualuser);
        String actval=actual.getText().substring(1, 5);
        if(actval.equals(user)) {
            System.out.println("both are equal");
        }else
        System.out.println("not equal");*/
        verify(actualuser, user);
    }

    public void turnoff() {
        click(logout);


    }

    public void verifyErrMsg() {
        System.out.println(getText(err_msg));
    }
    /*public void clearText(){


    }
}*/
}
