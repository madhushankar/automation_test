package pages;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;

/**
 * Created by Brajesh on 11/02/15.
 */
public class SearchProducts extends Page {


    By searchfield = By.name(" Search Products, Brands etc.");
    By search_Product = By.name("Search for Products..");
    By Filter = By.name("FILTER");
    By Sort = By.name("SORT BY");
    By variables = By.name("Watches (18873)");
    By Watches = By.partialLinkText("Watches");
    By Wrist_watch = By.partialLinkText("Wrist Watches");
    By Casio_watch = By.partialLinkText("Casio Watches");
    By Apply = By.name("APPLY");
    By Price_low_high = By.name("Price -- Low to High");


    /**
     * @param driver
     */
    public SearchProducts(AppiumDriver driver) {
        super(driver);
    }

    public SearchProducts filter() throws InterruptedException {

        click(searchfield);
        sendText(search_Product, "watches for men");
        enter();
        load();
        click(Filter);
        click(variables);
        click(Watches);
        click(Wrist_watch);
        click(Casio_watch);
        click(Apply);
        return this;
    }

    public SearchProducts Sort() {
        click(Sort);
        //  click(Price_low_high);
        return this;
    }

}
