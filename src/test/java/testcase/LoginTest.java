package testcase;


import org.testng.annotations.Test;
import pages.LoginPage;
import pages.Page;


/**
 * Created by Brajesh on 13/02/15.
 */
public class LoginTest extends BaseTest {

    /*
    login with valid credentials
     */

    @Test(priority = 1)
    public void logingInTest() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.setting();
        loginPage.navigateToLogin();
        loginPage.login("madhushankar15@gmail.com", "moolya123");
        loginPage.setting();
        loginPage.verifyuser("madhu");
        loginPage.turnoff();
    }

    /*log in with invalid credentials
      login without password
      login with wrong password
      login with nothing
     */
    @Test
    public void showErrorTest() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        Page page = new Page(driver);
        loginPage.setting();
        loginPage.navigateToLogin();
        /* login without password*/
        loginPage.login("madhushankar15@gmail.com", "");
        loginPage.verifyErrMsg();
        page.load();

        /*login without any credentials*/
        loginPage.login("", "");
        loginPage.verifyErrMsg();
        page.load();
        /*login with wrong credentials*/
        loginPage.login("madhushankar15@gmail.com", "moolya");
        loginPage.verifyErrMsg();

    }

}
