package testcase;
/*
  Android-Automation
  Created by Don Wettasinghe on 7/25/2014.
  Copyright (c) 2014 DrFirst. All rights reserved.
*/

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;


public class BaseTest {
    public static AppiumDriver driver;
    public static WebDriverWait wait;

    protected static String appUri = null;
    protected static String appiumServer = null;
    protected static String appPackage = null;
    protected static String appActivity = null;
    protected static String appiumVersion = null;
    protected static String platformName = null;
    protected static String platformVersion = null;
    protected static String deviceName = null;
    String appActivity1;

    @BeforeClass
    public static void loadConfig() {

        try {
            XMLConfiguration config = new XMLConfiguration("testconfig.xml");
            appUri = config.getString("appUri");
            appiumServer = config.getString("appiumServer");
            appPackage = config.getString("appPackage");
            appActivity = config.getString("appActivity");
            appiumVersion = config.getString("appiumVersion");
            platformName = config.getString("platformName");
            platformVersion = config.getString("platformVersion");
            deviceName = config.getString("deviceName");

        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    @BeforeMethod
    public void setup() throws Exception {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("appium-version", appiumVersion);
        capabilities.setCapability("platformName", platformName);
        capabilities.setCapability("platformVersion", platformVersion);
        capabilities.setCapability("deviceName", deviceName);
        capabilities.setCapability("app", appUri);
        capabilities.setCapability("appPackage", appPackage);
        capabilities.setCapability("appActivity", appActivity);
        driver = new AndroidDriver(
                new URL(appiumServer), capabilities);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 25);

    }


    @AfterMethod
    public void teardown() {
        driver.quit();

    }

    public void takeScreenShot(WebDriver driver) {
        try {
            String Screenshotpath = "/Users/Brajesh/Desktop/AppiumScreenshot";
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyDirectory(scrFile, new File(Screenshotpath + "ScreenShot.jpg"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
