package utils;

/**
 * Created by Brajesh on 18/02/15.
 */
public enum SortByTabs {
    POPULARITY(0), LOW_TO_HIGH(2), HIGH_TO_LOW(1), DISCOUNT(3), NEWEST_FIRST(4);
    private int indexId;

    SortByTabs(int indexId) {
        this.indexId = indexId;
    }

    public int indexId() {
        return indexId;
    }
}


